#!/usr/bin/python


import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter, MultipleLocator
import numpy as np


def graficoBS():
    n = 8
    a = np.arange(1, n + 1)
    x = 2**a
    # line 1 points
    y1 = [55.27, 43.82, 41.52, 37.88, 34.86, 33.32, 33.16, 34.04]
    # plotting the line 1 points
    plt.plot(x.astype("str"), y1, linestyle="--", marker="o", label="Cluster")

    # line 2 points
    y2 = [13.81, 7.53, 7.45, 9.17, 9.71, 10.79, 11.77, 12.20]
    # plotting the line 2 points
    plt.plot(x.astype("str"), y2, linestyle="--", marker="o", label="PC local")

    plt.gca().yaxis.set_major_locator(MultipleLocator(base=2.0))
    plt.gca().yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

    plt.xlabel("block size (BS)")
    plt.ylabel("tiempo promedio CANT=10 (s)")
    plt.title("Tiempo con N=2048")
    plt.legend()
    plt.savefig("images/graficoBS.png", bbox_inches="tight", dpi=600)
    plt.show()


def graficoSize():
    n = 4
    a = np.arange(9, 9 + n)
    x = 2**a
    # line 1 points
    y1 = [0.53, 4.29, 33.16, 271.73]
    # plotting the line 1 points
    plt.plot(x.astype("str"), y1, linestyle="--", marker="o", label="Cluster")

    # line 2 points
    y2 = [0.11, 0.94, 7.45, 59.75]
    # plotting the line 2 points
    plt.plot(x.astype("str"), y2, linestyle="--", marker="o", label="PC local")
    plt.gca().yaxis.set_major_locator(MultipleLocator(base=25.0))
    plt.gca().yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

    # naming the x axis
    plt.xlabel("tamaño de la matriz (N)")
    # naming the y axis
    plt.ylabel("tiempo promedio CANT=10 (s)")
    # giving a title to my graph
    plt.title("Tiempo con BS=8 en PC local y BS=128 en cluster")

    # show a legend on the plot
    plt.legend()

    # function to show the plot
    plt.savefig("images/graficoSize.png", bbox_inches="tight", dpi=600)
    plt.show()


def graficoFloatDouble(yfloat, ydouble, device, exercise,base):
    x = np.array([10, 32, 50, 100, 512, 1024])
    # plotting the line 1 points
    plt.plot(
        x.astype("str"), yfloat, linestyle="--", marker="o", label="%s Float" % device
    )

    # plotting the line 2 points
    plt.plot(
        x.astype("str"), ydouble, linestyle="--", marker="o", label="%s Double" % device
    )
    plt.gca().yaxis.set_major_locator(MultipleLocator(base=base))
    plt.gca().yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

    # naming the x axis
    plt.xlabel("tamaño de la macro TIMES")
    # naming the y axis
    plt.ylabel("tiempo promedio CANT=10 (s)")
    # giving a title to my graph
    plt.title("Tiempo de %s %s" % (device, exercise))

    # show a legend on the plot
    plt.legend()

    # function to show the plot
    plt.savefig(
        "images/graficoFloatDouble%s%s.png" % (device.replace(" ",""), exercise),
        bbox_inches="tight",
        dpi=600,
    )
    plt.show()


graficoBS()
graficoSize()

graficoFloatDouble(
    [0.602499, 1.925756, 3.012634, 6.023986, 30.77137675, 61.6140164],
    [0.695643, 2.221629, 3.477199, 6.958378, 35.21405875, 70.4007828],
    "Cluster",
    "quadratic2",
    5.0
)

graficoFloatDouble(
    [0.353150, 1.095486, 1.757879, 3.510056, 18.1104328, 35.9860074],
    [0.695643, 2.221629, 3.477199, 6.958378, 35.21405875, 70.4007828],
    "Cluster",
    "quadratic3",
    5.0
)


graficoFloatDouble(
    [0.098835, 0.318047, 0.509792, 1.004935, 5.071808, 10.239710],
    [0.086849, 0.289231, 0.435666, 0.856050, 4.407622, 8.743623],
    "PC local",
    "quadratic2",
    1.0
)

graficoFloatDouble(
    [0.060617, 0.194093, 0.375531, 0.619251, 3.096454, 6.284831],
    [0.086849, 0.289231, 0.435666, 0.856050, 4.407622, 8.743623],
    "PC local",
    "quadratic3",
    1.0
)
