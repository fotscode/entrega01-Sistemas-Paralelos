#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void matmulblks_double(double *a, double *b, double *c, int N, int BS,
                       double cociente);
void blkmul_double(double *ablk, double *bblk, double *cblk, int N, int BS,
                   double cociente);
void matmulblks_double_int(double *a, int *b, double *c, int N, int BS);
void blkmul_double_int(double *ablk, int *bblk, double *cblk, int N, int BS);

double dwalltime();

#define randnum(min, max) ((rand() % (int)(((max) + 1) - (min))) + (min))

/************Allocs, prints e inicializacion************/

double *alloc_double(int bs) { return malloc(sizeof(double) * bs); }

void init_vector(int *v, int N) {
  for (int i = 0; i < N; i++) {
    v[i] = -1;
  }
}

void init_matrix_int(int *m, int value, int N) {
  int i, j;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      m[j * N + i] = randnum(0, N - 1);
    }
  }
}

void init_matrix_double(double *m, double value, int row, int N) {
  int i, j;
  if (row == 0) {
    for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
        m[i * N + j] = 0;
        if (value != 0)
          m[i * N + j] = randnum(0, 10);
      }
    }
  } else {
    for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
        m[j * N + i] = 0;
        if (value != 0)
          m[j * N + i] = randnum(0, 10);
      }
    }
  }
}

void print_matrix_int(int *m, int N) {
  for (int i = 0; i < N; i++) {
    printf("[");
    for (int j = 0; j < N; j++) {
      printf("%5d,", m[j * N + i]);
    }
    puts("]");
  }
}

void print_matrix_double(double *m, int row, int N) {
  if (row == 0) {
    for (int i = 0; i < N; i++) {
      printf("[");
      for (int j = 0; j < N; j++) {
        printf("%8.2f,", m[i * N + j]);
      }
      puts("]");
    }
  } else {
    for (int i = 0; i < N; i++) {
      printf("[");
      for (int j = 0; j < N; j++) {
        printf("%8.2f,", m[j * N + i]);
      }
      puts("]");
    }
  }
}

void print_all(double *A, double *B, double *C, double *R, double *temp, int *D,
               double *res, double cociente, int N) {
  puts("Matrix A");
  print_matrix_double(A, 0, N);
  puts("Matrix B");
  print_matrix_double(B, 1, N);
  puts("Matrix C");
  print_matrix_double(C, 0, N);
  puts("Matrix D");
  print_matrix_int(D, N);
  puts("Matrix temp (resultado entre CxD)");
  print_matrix_double(temp, 0, N);
  printf("res (maxA,minA,promA,maxB,minB,promB)  \n[");
  for (int z; z < 6; z++) {
    printf(" %.2f ", res[z]);
  }
  printf("]\n");
  printf("cociente ([maxA*maxB-minA*minB]/[promA*promB]): %f\n", cociente);
  puts("Matrix R");
  print_matrix_double(R, 0, N);
}

/**************Pot2 especificado en el PDF**************/

void Pot2(int *d, int N, int *potencias) {
  int i, j, t, indice, fila;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      indice = fila + j;
      t = d[indice];
      if (potencias[t] == -1) {
        potencias[t] = t * t;
      }
      d[indice] = potencias[t];
    }
  }
}

/**********Calculo de maximo, minimo y promedio**********/

// res[0] maxA, res[1] minA, res[2] promA
// res[3] maxB, res[4] minB, res[5] promB
void max_min_prom(double *A, double *B, double *res, int N) {
  int i, j, fila, nxn;
  double valA, valB;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      valA = A[fila + j];
      valB = B[fila + j];
      if (res[0] < valA)
        res[0] = valA;
      if (res[1] > valA)
        res[1] = valA;
      res[2] += valA;
      if (res[3] < valB)
        res[3] = valB;
      if (res[4] > valB)
        res[4] = valB;
      res[5] += valB;
    }
  }
  nxn = N * N;
  res[2] = res[2] / (nxn);
  res[5] = res[5] / (nxn);
}

/*********Multiplicacion de matrices en bloques*********/

void matmulblks_double(double *a, double *b, double *c, int N, int BS,
                       double cociente) {
  int i, j, k, fila, col, res;
  for (i = 0; i < N; i += BS) {
    fila = i * N;
    for (j = 0; j < N; j += BS) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < N; k += BS) {
        blkmul_double(&a[fila + k], &b[col + k], &c[res], N, BS, cociente);
      }
    }
  }
}

void blkmul_double(double *ablk, double *bblk, double *cblk, int N, int BS,
                   double cociente) {
  int i, j, k, fila, col, res;
  for (i = 0; i < BS; i++) {
    fila = i * N;
    for (j = 0; j < BS; j++) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < BS; k++) {
        cblk[res] += ablk[fila + k] * bblk[col + k];
      }
      cblk[res] *= cociente;
    }
  }
}

void matmulblks_double_int(double *a, int *b, double *c, int N, int BS) {
  int i, j, k, fila, col, res;
  for (i = 0; i < N; i += BS) {
    fila = i * N;
    for (j = 0; j < N; j += BS) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < N; k += BS) {
        blkmul_double_int(&a[fila + k], &b[col + k], &c[res], N, BS);
      }
    }
  }
}

void blkmul_double_int(double *ablk, int *bblk, double *cblk, int N, int BS) {
  int i, j, k, fila, col, res;
  for (i = 0; i < BS; i++) {
    fila = i * N;
    for (j = 0; j < BS; j++) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < BS; k++) {
        cblk[res] += ablk[fila + k] * bblk[col + k];
      }
    }
  }
}

/*******************Agregar matrices*******************/

void matadd_double(double *a, double *b, int N) {
  int i, j, fila, indice;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      indice = fila + j;
      a[indice] += b[indice];
    }
  }
}

/**********************BEGIN MAIN**********************/

int main(int argc, char *argv[]) {
  int N, BS;

  // ∨∨∨∨ checkeo de parametros ∨∨∨∨
  if ((argc != 3) || ((N = atoi(argv[1])) <= 0) ||
      ((BS = atoi(argv[2])) <= 0) || ((N % BS) != 0)) {
    printf("Error en los parámetros. Usage: ./%s N BS (N debe ser multiplo "
           "de BS)\n",
           argv[0]);
    exit(1);
  }

#ifdef CANT /* si se compila con -D CANT=NUMERO */
  double promedio = 0;
  for (size_t i = 0; i < CANT; i++) {
#endif /* ifdef CANT */
    double *A, *B, *C, *R, *temp, *res, cociente, init_tick, final_tick;
    int *D, *potencias;
    // ∨∨∨∨ fase inicializacion ∨∨∨∨
    res = alloc_double(6);
    A = alloc_double(N * N);
    B = alloc_double(N * N);
    C = alloc_double(N * N);
    R = alloc_double(N * N);
    temp = alloc_double(N * N);
    D = (int *)malloc(sizeof(int) * N * N);
    potencias = (int *)malloc(sizeof(int) * N);
    res[0] = DBL_MIN;
    res[3] = DBL_MIN;
    res[1] = DBL_MAX;
    res[4] = DBL_MAX;
    res[2] = 0;
    res[5] = 0;
    init_matrix_double(A, 5, 0, N);
    init_matrix_double(B, 5, 1, N);
    init_matrix_double(C, 5, 0, N);
    init_matrix_double(temp, 0, 0, N);
    init_vector(potencias, N);
    init_matrix_int(D, 2, N);
    // ∨∨∨∨ fase procesamiento ∨∨∨∨
    init_tick = dwalltime();
    max_min_prom(A, B, res, N);
    cociente = (res[2] * res[5] != 0)
                   ? (res[0] * res[3] - res[1] * res[2]) / (res[2] * res[5])
                   : 0;
    matmulblks_double(A, B, R, N, BS, cociente);
    Pot2(D, N, potencias);
    matmulblks_double_int(C, D, temp, N, BS);
    matadd_double(R, temp, N);
    final_tick = dwalltime();
    // ∨∨∨∨ fase imprimir y liberar ∨∨∨∨
    printf("Tiempo en segundos %f \n", final_tick - init_tick);
#ifdef DEBUG
    print_all(A, B, C, R, temp, D, res, cociente, N);
#endif
    free(A);
    free(B);
    free(C);
    free(R);
    free(D);
    free(temp);
    free(res);
    free(potencias);
#ifdef CANT /* end del for */
    promedio += (final_tick - init_tick) / CANT;
  }
  printf("N: %d | BS: %d | Promedio: %f\n",N,BS, promedio);
#endif /* ifdef CANT */
  return 0;
}

/***********************END MAIN***********************/

#include <sys/time.h>

double dwalltime() {
  double sec;
  struct timeval tv;

  gettimeofday(&tv, NULL);
  sec = tv.tv_sec + tv.tv_usec / 1000000.0;
  return sec;
}
